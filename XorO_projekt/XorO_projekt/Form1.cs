﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XorO_projekt
{
    /*! \mainpage Strona głowna
     * 
     * \section intro_sec Wstęp
     * Tematem projektu jest realizownie gry „Kółko i Krzyżek” w języku C#, korzystując z technologij „Windows Forms” oraz korzystując z technologji „.NET”. Celem gry jest ułożyć 3 kółka lub krzyżka w jedną linję.
     */

    /**
     * Kółko i Krzyżek. 
     * Głuwna klassa projektu, zawierająca wszystkie elementy tego programu. 
     * 
     * 
     */
    ///@{
    ///Kłassa w której jest tworzone wszystkie panele, na których jest umieścione elementy zarządzania programą
    public partial class Form1 : Form
    {
        

        
        PictureBox[] GamePole = new PictureBox[9]; ///< Tabela przycisków dla grania 


        int Player = 0; ///< Zminna do śledzenia za graczem 
        int Computer = 0; ///< Zmienna do śledzenia za kumputerem

        int[] GamePoleMap = {
                             0,0,0,
                             0,0,0,
                             0,0,0
                         }; ///< Zmienna dla pokazywania elementów na polu gry

        string[] ImgName =
        {
            "field.png", //пустой блок
            "X.png", //крестик
            "O.png"  //нолик
        };///<Zmienna przychowywująca nazwe plików zdjęć 

        public Form1()
        {
            InitializeComponent();
        }
        /**
         *Funkcja do tworzenia pola dla gry (Pola z 9 przyciskami) 
         */
        void MainPole()
        {
            //задаем начало рисования поля
            int
                DX = 0,
                DY = 0;

            //размеры картинки
            int
                HeighP = 70, //высота
                WhidthP = 70,  //ширина  
                               //счетчик подсчета картинок
                IndexPicture = 0;
            //имя в ячейке будет начинаться с этих символов
            string NAME = "P_";

            //цикл расстановки ячеек по Y
            for (int YY = 0; YY < 3; YY++)
            {
                //цикл расстановки ячеек по X
                for (int XX = 0; XX < 3; XX++)
                {
                    GamePole[IndexPicture] = new PictureBox()
                    {
                        Name = NAME + IndexPicture,                 //Задаем имя картинки
                        Height = HeighP,                            //задаем размер по Y
                        Width = WhidthP,                            //задаем размер 
                        Image = Image.FromFile("field.png"),        //загружаем изображение пустого поля
                        SizeMode = PictureBoxSizeMode.StretchImage, //заставляем изображение сжаться по размерам картинки
                        Location = new Point(DX, DY)
                    };

                    GamePole[IndexPicture].Click += Picture_Click;

                    PoleGry.Controls.Add(GamePole[IndexPicture]); //размещаем картинку на пенале управления
                    //рассчитываем новое имя
                    IndexPicture++;

                    DX += WhidthP; //рассчитываем координаты по X для следующей картинки
                }
                DY += HeighP; //рассчитываем координаты по Y для следующей картинки
                DX = 0; //обнуляем позицию для координаты X
            }
        }


        /**
         *Tworzenie formy
        */
        private void Form1_Load(object sender, EventArgs e)
        {
            MainPole();

            Wybor.Location = new Point(27, 12);
            PoleGry.Location = new Point(27, 12);
            Resultat.Location = new Point(27, 12);
        }

        /** Funkcja wyjścia z programu
         *
         */

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /**
         * Funkcja wyboru krzyżka przez grającego
         */
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //игрок выбрал Х
            Player = 1;
            //компьютер играет О
            Computer = 2;
            //прячем 2 панель
            Wybor.Visible = false;
            //показываем поле игры на 3 панели
            PoleGry.Visible = true;
        }

        /**
         * Funkcja wyboru krzyżka przez komputer
         */
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //игрок выбрал Х
            Player = 2;
            //компьютер играет О
            Computer = 1;
            //прячем 2 панель
            Wybor.Visible = false;
            //показываем поле игры на 3 панели
            PoleGry.Visible = true;
        }

        /**
         * Funkcja przejścia do menu wyboru
         */
        private void button1_Click(object sender, EventArgs e)
        {
            Wybor.Visible = true;

            Menu.Visible = false;
        }

        /**
         * Funkcja zablokowania przycisków pola grającego
         */
        public bool LoockPole()
        {
            //блокируем все поле чтобы игрок не мог на него нажать
            foreach (PictureBox P in GamePole) P.Enabled = false;

            return false;
        }


        /**
         * Funkcja sprawdzenia tego kto wygrał
         * \param [in] who (numer grającego)
         */
        //функция проверки на победу передаем значение кого будем проверять
        public bool TestWin(int WHO)
        {
            //список вариантов выигрышных комбинаций
            int[,] WinVariant =
            {      {    //1 вариант
                    1,1,1,  //Х Х Х
                    0,0,0,  //_ _ _
                    0,0,0   //_ _ _
                }, {    //2 вариант
                    0,0,0,  //_ _ _
                    1,1,1,  //Х Х Х
                    0,0,0   //_ _ _
                }, {    //3 вариант
                    0,0,0,  //_ _ _
                    0,0,0,  //_ _ _
                    1,1,1   //Х Х Х
                }, {    //4 вариант
                    1,0,0,  //Х _ _
                    1,0,0,  //Х _ _
                    1,0,0   //Х _ _
                }, {    //5 вариант
                    0,1,0,  //_ Х _
                    0,1,0,  //_ Х _
                    0,1,0   //_ Х _
                }, {    //6 вариант
                    0,0,1,  //_ _ Х
                    0,0,1,  //_ _ Х
                    0,0,1   //_ _ Х
                }, {    //7 вариант
                    1,0,0,  //Х _ _
                    0,1,0,  //_ Х _
                    0,0,1   //_ _ Х
                }, {    //8 вариант
                    0,0,1,   //_ _ Х
                    0,1,0,   //_ Х _
                    1,0,0    //Х _ _
                }
            };

            //получаем  поле
            int[] TestMap = new int[GamePoleMap.Length];
            //просчитываем поле
            for (int I = 0; I < GamePoleMap.Length; I++)
                //если номер в ячейке нам подходит записываем в карту 1
                if (GamePoleMap[I] == WHO) TestMap[I] = 1;

            //выбираем вариант для сравнения 
            for (int Variant_Index = 0; Variant_Index < WinVariant.GetLength(0); Variant_Index++)
            {
                //счетчик для подсчета соотвествий
                int WinState = 0;
                for (int TestIndex = 0; TestIndex < TestMap.Length; TestIndex++)
                {
                    //если параметр равен 1 то проверяем его иначе 0 тоже = 0
                    if (WinVariant[Variant_Index, TestIndex] == 1)
                    {
                        //если в параметр  в варианте выигрыша совпал с вариантом на карте считаем это в параметре WinState
                        if (WinVariant[Variant_Index, TestIndex] == TestMap[TestIndex]) WinState++;
                    }
                    //если найдены 3 совпадения значит это и есть выигрышная комбинация
                    if (WinState == 3) return true;
                }

            }

            return false;

        }

        /**
         * Funkcja do sprawdzenia czy jest krok następny osoby
         */
        public bool CanStap()
        {
            //перебираем все значения игрового поля
            foreach (int s in GamePoleMap)
                //если нашли 0 значит есть куда ходить
                if (s == 0) return true;

            //проверяем не выиграл ли игрок
            if (TestWin(Player))
            {
                label3.Text = "You Win";
                LoockPole();
                //прячем панель игры
                Resultat.Visible = true;
                PoleGry.Visible = false;
                //если не нашли то ходить больше нельзя
                return false;
            }
            //проверяем не выиграл ли игрок
            if (TestWin(Computer))
            {
                label3.Text = "You Lose";
                Resultat.Visible = true;
                //прячем панель игры
                LoockPole();
                PoleGry.Visible = false;
                return false;
            }


            //если ходить больше нельзя и никто не выиграл значит пишем что ничья
            label3.Text = "Nobody's Win";
            //прячем панель игры
           
            Resultat.Visible = true;
            PoleGry.Visible = false;
            LoockPole();


            return false;
        }

        /**
         * Funkcja do sprawdzenia czy jest krok następny komputera
         */
        void PC_Step()
        {

            //объявляем функцию генерации случайных чисел 
            Random Rand = new Random();
        GENER:

            if (CanStap())
            {
                //получаем число от 0 до 8
                int IndexStep = Rand.Next(0, 8);

                //смотрим если ячейка пуста то ставим туда символ ПК
                if (GamePoleMap[IndexStep] == 0)
                {
                    //рисуем нужную картинку
                    GamePole[IndexStep].Image = Image.FromFile(ImgName[Computer]);
                    //записываем в поле игры ход компьютера
                    GamePoleMap[IndexStep] = Computer;

                }
                else goto GENER;
                if (TestWin(Computer))
                {
                    Resultat.Visible = true;
                    label3.Text = "You Lose";
                    PoleGry.Visible = false;
                }
            }
        }

        /**
         * Funkcja do odbłokowania pola
         */
        void UnLoockPole()
        {
            int Indexx = 0;
            //разблокируем поля но только те которые не заполнены
            foreach (PictureBox P in GamePole)
            {
                //если поле равно 0 значит есть смысл его открывать
                if (GamePoleMap[Indexx++] == 0) P.Enabled = true;
            }
        }


        /**
         * Funkcja do wrucenia w menu glówne
         */
        private void button3_Click(object sender, EventArgs e)
        {
        
                //прячем панель 4
                Resultat.Visible = false;
                //прячем панель 3
                PoleGry.Visible = false;
                //обнуляем карту игры
                GamePoleMap = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                //обнуляем изображение поля
                foreach (PictureBox P in GamePole) P.Image = Image.FromFile(ImgName[0]);
                //обнуляем выбор игрока
                Player = 0;
                //обнуляем выбор ПК
                Computer = 0;
                //пробуем разблокировать поле 
                UnLoockPole();
                //показываем меню игры
                Menu.Visible = true;
            
        }
        /**
         * Funkcja do wpisania w przycisk kółko albo krzyżek
         */
        private void Picture_Click(object sender, EventArgs e)
        {
            if (CanStap())
            {
                PictureBox ClickImage = sender as PictureBox;
                string[] ParsName = ClickImage.Name.Split('_');

                int IndexSelectImage = Convert.ToInt32(ParsName[1]);

                GamePole[IndexSelectImage].Image = Image.FromFile(ImgName[Player]);
                GamePoleMap[IndexSelectImage] = Player;

                if (!TestWin(Player))
                {
                    //блокируем поле чтобы игрок не смог ходить
                    LoockPole();
                    //Шаг ПК
                    PC_Step();
                    //пробуем разблокировать поле 
                    UnLoockPole();
                }
                else
                {
                    Resultat.Visible = true;
                    label3.Text = "You Win";
                    PoleGry.Visible = false;
                    LoockPole();
                }
            }
        }
    }
}///@}
