﻿namespace XorO_projekt
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Menu = new System.Windows.Forms.Panel();
            this.lMenu = new System.Windows.Forms.Label();
            this.exit = new System.Windows.Forms.Button();
            this.Start = new System.Windows.Forms.Button();
            this.Wybor = new System.Windows.Forms.Panel();
            this.O = new System.Windows.Forms.PictureBox();
            this.X = new System.Windows.Forms.PictureBox();
            this.lXorO = new System.Windows.Forms.Label();
            this.PoleGry = new System.Windows.Forms.Panel();
            this.Resultat = new System.Windows.Forms.Panel();
            this.Back = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Menu.SuspendLayout();
            this.Wybor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.O)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X)).BeginInit();
            this.Resultat.SuspendLayout();
            this.SuspendLayout();
            // 
            // Menu
            // 
            this.Menu.Controls.Add(this.lMenu);
            this.Menu.Controls.Add(this.exit);
            this.Menu.Controls.Add(this.Start);
            this.Menu.Location = new System.Drawing.Point(13, 13);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(268, 250);
            this.Menu.TabIndex = 0;
            // 
            // lMenu
            // 
            this.lMenu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lMenu.Location = new System.Drawing.Point(88, 44);
            this.lMenu.Name = "lMenu";
            this.lMenu.Size = new System.Drawing.Size(100, 23);
            this.lMenu.TabIndex = 2;
            this.lMenu.Text = "Main Menu";
            this.lMenu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(91, 167);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(97, 23);
            this.exit.TabIndex = 1;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.button2_Click);
            // 
            // Start
            // 
            this.Start.Location = new System.Drawing.Point(91, 108);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(97, 23);
            this.Start.TabIndex = 0;
            this.Start.Text = "Start";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.button1_Click);
            // 
            // Wybor
            // 
            this.Wybor.Controls.Add(this.O);
            this.Wybor.Controls.Add(this.X);
            this.Wybor.Controls.Add(this.lXorO);
            this.Wybor.Location = new System.Drawing.Point(312, 13);
            this.Wybor.Name = "Wybor";
            this.Wybor.Size = new System.Drawing.Size(268, 250);
            this.Wybor.TabIndex = 1;
            this.Wybor.Visible = false;
            // 
            // O
            // 
            this.O.Image = ((System.Drawing.Image)(resources.GetObject("O.Image")));
            this.O.Location = new System.Drawing.Point(150, 100);
            this.O.Name = "O";
            this.O.Size = new System.Drawing.Size(90, 90);
            this.O.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.O.TabIndex = 5;
            this.O.TabStop = false;
            this.O.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // X
            // 
            this.X.Image = ((System.Drawing.Image)(resources.GetObject("X.Image")));
            this.X.Location = new System.Drawing.Point(28, 100);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(90, 90);
            this.X.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.X.TabIndex = 4;
            this.X.TabStop = false;
            this.X.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // lXorO
            // 
            this.lXorO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lXorO.Location = new System.Drawing.Point(90, 44);
            this.lXorO.Name = "lXorO";
            this.lXorO.Size = new System.Drawing.Size(100, 23);
            this.lXorO.TabIndex = 3;
            this.lXorO.Text = "X or O";
            this.lXorO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PoleGry
            // 
            this.PoleGry.Location = new System.Drawing.Point(38, 287);
            this.PoleGry.Name = "PoleGry";
            this.PoleGry.Size = new System.Drawing.Size(268, 250);
            this.PoleGry.TabIndex = 2;
            this.PoleGry.Visible = false;
            // 
            // Resultat
            // 
            this.Resultat.Controls.Add(this.Back);
            this.Resultat.Controls.Add(this.label3);
            this.Resultat.Location = new System.Drawing.Point(312, 287);
            this.Resultat.Name = "Resultat";
            this.Resultat.Size = new System.Drawing.Size(268, 250);
            this.Resultat.TabIndex = 3;
            this.Resultat.Visible = false;
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(93, 145);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(97, 23);
            this.Back.TabIndex = 5;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(90, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 4;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 603);
            this.Controls.Add(this.Resultat);
            this.Controls.Add(this.PoleGry);
            this.Controls.Add(this.Wybor);
            this.Controls.Add(this.Menu);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Menu.ResumeLayout(false);
            this.Wybor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.O)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X)).EndInit();
            this.Resultat.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Menu;
        private System.Windows.Forms.Label lMenu;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.Panel Wybor;
        private System.Windows.Forms.Label lXorO;
        private System.Windows.Forms.Panel PoleGry;
        private System.Windows.Forms.Panel Resultat;
        private System.Windows.Forms.PictureBox O;
        private System.Windows.Forms.PictureBox X;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Label label3;
    }
}

