﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XorO_projekt
{
    /*! \mainpage Strona głowna
     * 
     * \section intro_sec Wstęp
     * Tematem projektu jest realizownie gry „Kółko i Krzyżek” w języku C#, korzystując z technologij „Windows Forms” oraz korzystując z technologji „.NET”. Celem gry jest ułożyć 3 kółka lub krzyżka w jedną linję.
     */

    /**
     * Kółko i Krzyżek. 
     * Głuwna klassa projektu, zawierająca wszystkie elementy tego programu. 
     * 
     * 
     */
    ///Kłassa w której jest tworzone wszystkie panele, na których jest umieścione elementy zarządzania programą
    public partial class Form1 : Form
    {



        PictureBox[] GamePole = new PictureBox[9]; /// Tabela przycisków dla grania 


        int Player = 0; /// Zminna do śledzenia za graczem 
        int Computer = 0; /// Zmienna do śledzenia za kumputerem

        int[] GamePoleMap = {
                             0,0,0,
                             0,0,0,
                             0,0,0
                         }; /// Zmienna dla pokazywania elementów na polu gry

        string[] ImgName =
        {
            "field.png", //pusty blok
            "X.png", //krzyzek
            "O.png"  //kolko
        };///Zmienna przychowywująca nazwe plików zdjęć 

        public Form1()
        {
            InitializeComponent();
        }
        /**
         *Funkcja do tworzenia pola dla gry (Pola z 9 przyciskami) 
         */
        void MainPole()
        {
            //rysowanie pola
            int
                DX = 0,
                DY = 0;

            //rozmiar 
            int
                HeighP = 70,
                WhidthP = 70,
                //indeks do licenia zdjec
                IndexPicture = 0;
            //nazwa bedzie poczynala z takego
            string NAME = "P_";


            for (int YY = 0; YY < 3; YY++)
            {

                for (int XX = 0; XX < 3; XX++)
                {
                    GamePole[IndexPicture] = new PictureBox()
                    {
                        Name = NAME + IndexPicture,
                        Height = HeighP,
                        Width = WhidthP,
                        Image = Image.FromFile("field.png"),
                        SizeMode = PictureBoxSizeMode.StretchImage,
                        Location = new Point(DX, DY)
                    };

                    GamePole[IndexPicture].Click += Picture_Click;

                    PoleGry.Controls.Add(GamePole[IndexPicture]);

                    IndexPicture++;

                    DX += WhidthP;
                }
                DY += HeighP;
                DX = 0;
            }
        }


        /**
         *Tworzenie formy
        */
        private void Form1_Load(object sender, EventArgs e)
        {
            MainPole();

            Wybor.Location = new Point(27, 12);
            PoleGry.Location = new Point(27, 12);
            Resultat.Location = new Point(27, 12);
        }

        /** Funkcja wyjścia z programu
         *
         */

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /**
         * Funkcja wyboru krzyżka przez grającego
         */
        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Player = 1;

            Computer = 2;

            Wybor.Visible = false;

            PoleGry.Visible = true;
        }

        /**
         * Funkcja wyboru krzyżka przez komputer
         */
        private void pictureBox2_Click(object sender, EventArgs e)
        {

            Player = 2;

            Computer = 1;

            Wybor.Visible = false;

            PoleGry.Visible = true;
        }

        /**
         * Funkcja przejścia do menu wyboru
         */
        private void button1_Click(object sender, EventArgs e)
        {
            Wybor.Visible = true;

            Menu.Visible = false;
        }

        /**
         * Funkcja zablokowania przycisków pola grającego
         */
        public bool LoockPole()
        {

            foreach (PictureBox P in GamePole) P.Enabled = false;

            return false;
        }


        /**
         * Funkcja sprawdzenia tego kto wygrał
         * \param [in] who (numer grającego)
         */

        public bool TestWin(int WHO)
        {

            int[,] WinVariant =
            {      {    //1 
                    1,1,1,  //Х Х Х
                    0,0,0,  //_ _ _
                    0,0,0   //_ _ _
                }, {    //2 
                    0,0,0,  //_ _ _
                    1,1,1,  //Х Х Х
                    0,0,0   //_ _ _
                }, {    //3 
                    0,0,0,  //_ _ _
                    0,0,0,  //_ _ _
                    1,1,1   //Х Х Х
                }, {    //4 
                    1,0,0,  //Х _ _
                    1,0,0,  //Х _ _
                    1,0,0   //Х _ _
                }, {    //5 
                    0,1,0,  //_ Х _
                    0,1,0,  //_ Х _
                    0,1,0   //_ Х _
                }, {    //6 
                    0,0,1,  //_ _ Х
                    0,0,1,  //_ _ Х
                    0,0,1   //_ _ Х
                }, {    //7 
                    1,0,0,  //Х _ _
                    0,1,0,  //_ Х _
                    0,0,1   //_ _ Х
                }, {    //8 
                    0,0,1,   //_ _ Х
                    0,1,0,   //_ Х _
                    1,0,0    //Х _ _
                }
            };


            int[] TestMap = new int[GamePoleMap.Length];

            for (int I = 0; I < GamePoleMap.Length; I++)

                if (GamePoleMap[I] == WHO) TestMap[I] = 1;


            for (int Variant_Index = 0; Variant_Index < WinVariant.GetLength(0); Variant_Index++)
            {

                int WinState = 0;
                for (int TestIndex = 0; TestIndex < TestMap.Length; TestIndex++)
                {

                    if (WinVariant[Variant_Index, TestIndex] == 1)
                    {

                        if (WinVariant[Variant_Index, TestIndex] == TestMap[TestIndex]) WinState++;
                    }

                    if (WinState == 3) return true;
                }

            }

            return false;

        }

        /**
         * Funkcja do sprawdzenia czy jest krok następny osoby
         */
        public bool CanStap()
        {

            foreach (int s in GamePoleMap)

                if (s == 0) return true;


            if (TestWin(Player))
            {
                label3.Text = "You Win";
                LoockPole();

                Resultat.Visible = true;
                PoleGry.Visible = false;

                return false;
            }

            if (TestWin(Computer))
            {
                label3.Text = "You Lose";
                Resultat.Visible = true;

                LoockPole();
                PoleGry.Visible = false;
                return false;
            }



            label3.Text = "Nobody's Win";


            Resultat.Visible = true;
            PoleGry.Visible = false;
            LoockPole();


            return false;
        }

        /**
         * Funkcja do sprawdzenia czy jest krok następny komputera
         */
        void PC_Step()
        {


            Random Rand = new Random();
        GENER:

            if (CanStap())
            {

                int IndexStep = Rand.Next(0, 8);


                if (GamePoleMap[IndexStep] == 0)
                {

                    GamePole[IndexStep].Image = Image.FromFile(ImgName[Computer]);

                    GamePoleMap[IndexStep] = Computer;

                }
                else goto GENER;
                if (TestWin(Computer))
                {
                    Resultat.Visible = true;
                    label3.Text = "You Lose";
                    PoleGry.Visible = false;
                }
            }
        }

        /**
         * Funkcja do odbłokowania pola
         */
        void UnLoockPole()
        {
            int Indexx = 0;

            foreach (PictureBox P in GamePole)
            {

                if (GamePoleMap[Indexx++] == 0) P.Enabled = true;
            }
        }


        /**
         * Funkcja do wrucenia w menu glówne
         */
        private void button3_Click(object sender, EventArgs e)
        {


            Resultat.Visible = false;

            PoleGry.Visible = false;

            GamePoleMap = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            foreach (PictureBox P in GamePole) P.Image = Image.FromFile(ImgName[0]);

            Player = 0;

            Computer = 0;

            UnLoockPole();

            Menu.Visible = true;

        }
        /**
         * Funkcja do wpisania w przycisk kółko albo krzyżek
         */
        private void Picture_Click(object sender, EventArgs e)
        {
            if (CanStap())
            {
                PictureBox ClickImage = sender as PictureBox;
                string[] ParsName = ClickImage.Name.Split('_');

                int IndexSelectImage = Convert.ToInt32(ParsName[1]);

                GamePole[IndexSelectImage].Image = Image.FromFile(ImgName[Player]);
                GamePoleMap[IndexSelectImage] = Player;

                if (!TestWin(Player))
                {

                    LoockPole();

                    PC_Step();

                    UnLoockPole();
                }
                else
                {
                    Resultat.Visible = true;
                    label3.Text = "You Win";
                    PoleGry.Visible = false;
                    LoockPole();
                }
            }
        }
    }
}
