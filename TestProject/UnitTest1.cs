using Microsoft.VisualStudio.TestTools.UnitTesting;
using XorO_projekt;

namespace TestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestLoockPanel()
        {
            Metods m = new Metods();
            
            Assert.AreEqual(m.LockPole(1), false);
        }

        [TestMethod]
        public void TestCanStep()
        {
            Metods m = new Metods();

            Assert.AreEqual(m.CanStep(2), false);
        }

        [TestMethod]
        public void TestTestWin()
        {
            Metods m = new Metods();

            Assert.AreEqual(m.TestWin(), true);
        }

    }
}
